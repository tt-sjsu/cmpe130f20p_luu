

```
                        CMPE130 MAP_ROUTING Project 
                               Fall 2020
                        Professor: Gokay Saldamli
                  Team THT: Thuy Luu, Hoang Tran, Thao Ton

Description: Map-routing-read the map from the US and calculate the shortest path between the starting point and other locations. 
             We use Dijkstra's algorithm with piority queue method.
    
```
### Install requirment packet
```buildoutcfg
    pip3 install matplotlib
```
### How to run  program ###
```buildoutcfg
    python3 map_routing.py 

```
### What is this repository for? ###

https://bitbucket.org/tt-sjsu/cmpe130f20p_luu.git

